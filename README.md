# Xano Stripe Prices

This is a demo of the Xano integration of Stripe Prices utilizing the session object.

## Core Structure
The folder structure is intended to be clean as possible. Specs have been removed.

### Setup & Dependencies
This boilerplate is intended to be as light as possible so outside of the core 
Angular packages only the following packages are included:

#### List of Dependencies
* material
* bootstrap css
* lodash-es
* stripe-angular

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

No test


## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
