import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {StripeService} from "../stripe.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';
import {getAllISOCodes} from "iso-country-currency";
import {BehaviorSubject} from "rxjs";

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {

	public isoCodes = getAllISOCodes();
	public pricingModel = 'standard';
	public recurring: boolean;
	public priceMetaData =  new BehaviorSubject([]);
	public productMetaData = new BehaviorSubject([]);
	public removedPriceMetaData = [];
	public removedProductMetaData = [];
	public tiersData = new BehaviorSubject([]);

	public priceForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		currency: new FormControl('', Validators.required),
		unit_amount: new FormControl(null),
		active: new FormControl(true),
		metadata: new FormControl(null),
		nickname: new FormControl(null),
		product: new FormControl(null),
		recurring: new FormGroup({
			interval: new FormControl(null),
			aggregate_usage: new FormControl(null),
			interval_count: new FormControl(null),
			usage_type: new FormControl(null)
		}),
		tiers: new FormControl(null),
		tiers_mode: new FormControl(null),
		billing_scheme: new FormControl('per_unit'),
		lookup_key: new FormControl(null),
		product_data: new FormGroup({
			name: new FormControl(null),
			active: new FormControl(null),
			metadata: new FormControl(null),
			statement_descriptor: new FormControl(null),
			unit_label: new FormControl(null)
		}),
		transfer_lookup_key: new FormControl(null),
		transform_quantity: new FormGroup({
			divide_by: new FormControl(null),
			round: new FormControl(null)
		}),
		unit_amount_decimal: new FormControl(null),
	})

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private stripeService: StripeService,
		private snackBar: MatSnackBar,
		private dialogRef: MatDialogRef<ManagePanelComponent>
	) {
	}

	ngOnInit(): void {
		if (this.data?.price) {
			this.tiersData.next(this.data.price?.tiers)
			this.priceForm.patchValue({
				id: this.data.price?.id,
				currency: this.data.price?.currency?.toUpperCase(),
				unit_amount: this.data.price?.unit_amount,
				active: this.data.price?.active,
				nickname: this.data.price?.nickname,
				product: this.data.price?.product?.id,
				recurring:{
					interval: this.data.price?.recurring?.interval,
					aggregate_usage: this.data.price?.recurring?.aggregate_usage,
					interval_count: this.data.price?.recurring?.interval_count,
					usage_type: this.data.price?.recurring?.usage_type
				},
				tiers_mode: this.data.price?.tiers_mode,
				billing_scheme:  this.data.price?.billing_scheme,
				lookup_key: this.data.price?.lookup_key,
				product_data:{
					name: this.data.price?.product_data?.name,
					active: this.data.price?.product_data?.active,
					statement_descriptor: this.data.price?.product_data?.statement_descriptor,
					unit_label: this.data.price?.product_data?.unit_label
				},
				transfer_lookup_key: this.data.price?.transfer_lookup_key,
				transform_quantity:{
					divide_by: this.data.price?.transform_quantity?.divide_by,
					round: this.data.price?.transform_quantity?.round
				},
				unit_amount_decimal: this.data.price?.unit_amount_decimal,
			});

			if(this.data.price?.transform_quantity) {
				this.pricingModel = 'package'
			} else if(this.data.price?.tiers_mode) {
				this.pricingModel = this.data.price?.tiers_mode;
			}

			if(this.data.price?.metadata) {
				let metadata = [];
				for (let x in this.data.price.metadata) {
					metadata.push({key: x, value: this.data.price.metadata[x]})
				}
				this.priceMetaData.next(metadata)
			}

			for(let control in this.priceForm.controls) {
				let availableEditControl = ['active', 'metadata', 'nickname', 'lookup_key', 'transfer_lookup_key'];
				if(!availableEditControl.includes(control)) {
					this.priceForm.controls[control].disable()
				}
			}
		}
	}

	public submit(): void {
		this.priceForm.markAllAsTouched();

		if (this.priceForm.valid) {
			this.priceForm.controls.active.patchValue(this.priceForm.controls.active.value.toString());

			if(this.priceForm.controls.active.value) {
				this.priceForm.controls.active.patchValue(this.priceForm.controls.active.value.toString());
			}

			// @ts-ignore
			if(this.priceForm.controls.product_data.controls.active.value) {
				// @ts-ignore
				this.priceForm.controls.product_data.controls.active.patchValue(this.priceForm.controls.product_data.controls.active.value.toString());
			}

			if (this.priceForm.controls.billing_scheme.value === 'tiered') {
				this.priceForm.controls.tiers.patchValue(this.tiersData.value)
				this.priceForm.controls.transform_quantity.reset();
			} else {
				this.priceForm.controls.tiers.reset()
			}

			if(this.priceMetaData?.value?.length) {
				let metadataObject = {};
				for(let x of this.priceMetaData.value) {
					let keyValue  = {};
					keyValue[x.key] = x.value;
					Object.assign(metadataObject,  keyValue);
				}
				for(let x of this.removedPriceMetaData) {
					let keyValue  = {};
					keyValue[x.key] = '';
					Object.assign(metadataObject,  keyValue);
				}
				this.priceForm.controls.metadata.patchValue(metadataObject)
			}

			if(this.productMetaData?.value?.length) {
				let metadataObject = {};
				for(let x of this.productMetaData.value) {
					let keyValue  = {};
					keyValue[x.key] = x.value;
					Object.assign(metadataObject, keyValue)
				}
				for(let x of this.removedProductMetaData) {
					let keyValue  = {};
					keyValue[x.key] = '';
					Object.assign(metadataObject,  keyValue);
				}
				// @ts-ignore
				this.priceForm.controls.product_data.controls.metadata.patchValue(metadataObject)
			}

			if (this.data?.price) {
				this.stripeService.updatePrice(this.data.price.id, {price: this.priceForm.getRawValue()}).subscribe(res => {
					this.snackBar.open('Price', 'Updated');
					this.dialogRef.close({updated: true, item: res});
				}, error => this.snackBar.open(get(error, 'error.message', 'An error occurred!'), 'Error', {panelClass: 'error-snack'}));
			} else {
				this.stripeService.savePrice({price: this.priceForm.getRawValue()}).subscribe(res => {
					this.snackBar.open('New Price', 'Saved');
					this.dialogRef.close({new: true, item: res})
				}, error =>  this.snackBar.open(get(error, 'error.message', 'An error occurred!'), 'Error', {panelClass: 'error-snack'}));

			}
		}
	}

	public modelChange(event) {
		this.pricingModel = event.value;
		switch (event.value) {
			case 'standard':
				this.priceForm.controls.billing_scheme.patchValue('per_unit');
				this.priceForm.controls.tiers_mode.patchValue(null);
				this.priceForm.controls.transform_quantity.reset()
				this.recurring = false;
				this.tiersData.next([]);
				break;
			case 'package':
				this.priceForm.controls.billing_scheme.patchValue('per_unit');
				this.priceForm.controls.tiers_mode.patchValue(null);
				this.recurring = false;
				this.tiersData.next([]);
				break;
			case 'graduated':
				this.priceForm.controls.billing_scheme.patchValue('tiered');
				this.priceForm.controls.tiers_mode.patchValue('graduated');
				this.priceForm.controls.transform_quantity.reset()
				this.recurring = true
				// @ts-ignore
				this.priceForm.controls.recurring.controls.interval.patchValue('day');
				if(!this.tiersData?.value?.length){
					this.tiersData.next([
						{first_item: 0, up_to: 1, unit_amount_decimal: null, flat_amount: null},
						{first_item: 1, up_to: 'inf', unit_amount_decimal: null, flat_amount: null}
					]);
				}
				break;
			case 'volume':
				this.priceForm.controls.billing_scheme.patchValue('tiered');
				this.priceForm.controls.tiers_mode.patchValue('volume');
				this.priceForm.controls.transform_quantity.reset()
				if(!this.tiersData?.value?.length){
					this.tiersData.next([
						{first_item: 0, up_to: 1, unit_amount_decimal: null, flat_amount: null},
						{first_item: 1, up_to: 'inf', unit_amount_decimal: null, flat_amount: null}
					]);
					this.recurring = true
					// @ts-ignore
					this.priceForm.controls.recurring.controls.interval.patchValue('day');
				}
				break;
			default: break;
		}
	}

	addTier() {
		let tiers =  [...this.tiersData.value];
		let up_to = parseInt(tiers[tiers.length -2].up_to)
		tiers.splice(
			tiers.length -1,
			0,
			{
				first_item: up_to + 1,
				up_to: parseInt(tiers[tiers.length -1].first_item) > up_to + 1 ? parseInt(tiers[tiers.length -1].first_item) : up_to + 2,
				unit_amount_decimal: null,
				flat_amount: null
			});

		tiers[tiers.length - 1].first_item = tiers[tiers.length - 2].up_to
		this.tiersData.next(tiers);
	}

	removeTier(row) {
		let tiers =  [...this.tiersData.value];

		tiers = tiers.filter(x => {
			if(!this.shallowEqual(row, x)) {
				return x;
			}
		});
		this.tiersData.next(tiers)
	}

	addMetadata(type) {
		if(type === 'price') {
			this.priceMetaData.next([...this.priceMetaData.value, {key: '', value: ''}])
		} else {
			this.productMetaData.next([...this.productMetaData.value,  {key: '', value: ''}])
		}
	}

	removeMetadata(row, type) {
		let metadata
		if(type === 'price') {
			 metadata = [...this.priceMetaData.value];
		} else {
			metadata = [...this.productMetaData.value]
		}

		metadata = metadata.filter(x => {
			if(!this.shallowEqual(row, x)) {
				return x;
			} else {
				if(type === 'price'){
					this.removedPriceMetaData.push(x)
				} else {
					this.removedProductMetaData.push(x)
				}
			}
		});

		if(type === 'price') {
			this.priceMetaData.next(metadata);
		} else {
			this.productMetaData.next(metadata);
		}
	}


	public shallowEqual(object1, object2) {
		const keys1 = Object.keys(object1);
		const keys2 = Object.keys(object2);

		if (keys1.length !== keys2.length) {
			return false;
		}

		for (let key of keys1) {
			if (object1[key] !== object2[key]) {
				return false;
			}
		}

		return true
	}
}
