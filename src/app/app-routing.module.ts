import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {PricesComponent} from "./prices/prices.component";

const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'sessions', component: PricesComponent},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
