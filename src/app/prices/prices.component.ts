import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StripeService} from "../stripe.service";
import {MatDialog} from "@angular/material/dialog";
import {ViewPanelComponent} from "../view-panel/view-panel.component";
import {Router} from "@angular/router";
import {ConfigService} from "../config.service";
import {MatPaginator} from "@angular/material/paginator";
import {finalize, startWith, switchMap, tap} from "rxjs/operators";

@Component({
	selector: 'app-prices',
	templateUrl: './prices.component.html',
	styleUrls: ['./prices.component.scss']
})
export class PricesComponent implements OnInit {

	public prices: any[] = []
	public displayedColumns: string[] = ['id', 'unit_amount', 'created'];
	public hasMore: boolean;
	public currentPageSize: number = 10;
	public loading: boolean;

	@ViewChild(MatPaginator) paginator: MatPaginator;

	constructor(
		private configService: ConfigService,
		private stripeService: StripeService,
		private dialog: MatDialog,
		private router: Router
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res=> {
			if(!res) this.router.navigate([''])
		});

		this.configService.updateResults.asObservable().subscribe(res=>{
			if(res) {
				this.getPrices();
				this.configService.updateResults.next(false);
			}
		});

		this.getPrices();
	}


	public pageResults(event) {
		if(this.currentPageSize === event.pageSize) {
			let paging = {}
			if (event.previousPageIndex > event.pageIndex) {
				paging['ending_before'] = this.prices[0].id
			} else {
				paging['starting_after'] = this.prices[this.prices.length - 1].id
			}
			this.stripeService.getPrices({limit: this.currentPageSize, ...paging})
				.subscribe(res => {
					this.hasMore = res.has_more;
					this.prices = res.data
				});
		} else {
			this.currentPageSize = event.pageSize
			this.stripeService.getPrices({limit: event.pageSize})
				.subscribe(res => {
					this.hasMore = res.has_more;
					this.prices = res.data
				});
		}
	}

	public viewPrice(price) {
		const dialogRef = this.dialog.open(ViewPanelComponent, {data: {price}});
		dialogRef.afterClosed().subscribe(res => {
			this.getPrices();
		});
	}

	public getPrices() {
		this.stripeService.getPrices({limit: this.currentPageSize})
			.pipe(
				tap(()=> this.loading = true),
				finalize(()=> this.loading = false ))
			.subscribe(res => {
				this.hasMore = res.has_more;
				this.prices = res.data
			});
	}
}
