import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class StripeService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public getPrices(prices): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/prices`,
			params: prices
		});
	}

	public getPrice(id: string): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/prices/${id}`,
		});
	}

	public savePrice(price): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/prices`,
			params: price,
		});
	}

	public updatePrice(id, price): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/prices/${id}`,
			params: price,
		});
	}

	public deletePrice(id: string): Observable<any> {
		return this.apiService.delete( {
			endpoint: `${this.configService.xanoApiUrl.value}/prices/${id}`
		})
	}
}
