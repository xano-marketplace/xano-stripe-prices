import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {StripeService} from "../stripe.service";
import {ManagePanelComponent} from "../manage-panel/manage-panel.component";

@Component({
	selector: 'app-view-panel',
	templateUrl: './view-panel.component.html',
	styleUrls: ['./view-panel.component.scss']
})
export class ViewPanelComponent implements OnInit {

	public result: any;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private stripeService: StripeService,
		private dialog: MatDialog,
		private dialogRef: MatDialogRef<ViewPanelComponent>
	) {
	}

	ngOnInit(): void {
		if(this.data?.price?.id) {
			this.stripeService.getPrice(this.data.price.id).subscribe(res => {
				this.result = res;
			});
		}
	}

	public edit(): void {
		const dialogRef = this.dialog.open(ManagePanelComponent, {data: {price: this.result}, minWidth: '25vw'})
		dialogRef.afterClosed().subscribe(res => {
			if(res?.updated) {
				this.stripeService.getPrice(this.data.price.id).subscribe(res => {
					this.result = res;
				});
			} else if(res?.deleted) {
				this.dialogRef.close();
			}
		});
	}
}
