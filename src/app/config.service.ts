import {Inject, Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {DOCUMENT} from "@angular/common";

declare let Stripe;

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>( null);
	public updateResults: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public stripe;
	public config: XanoConfig = {
		title: 'Xano Stripe Prices',
		summary: 'This demo illustrates the use of the Stripe Prices extension. ' +
			'The API mirrors all the Stripe Prices endpoints and includes a webhook endpoint for the price object.' +
			' You can create, update, and delete prices linked to your Stripe account.\n',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-stripe-prices',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
                	This demo illustrates the use of the Stripe Prices extension. The API mirrors all the Stripe 
                	Prices endpoints and includes a webhook endpoint for the price object. You can create, update,
                	and delete prices linked to your Stripe account.
								</p>
			    <h2 class="mt-2">Directions</h2>
				<ul>
				    <li>Install the extension in your Xano workspace.</li>
					<li>Create a <a href="https://stripe.com" target="_blank">Stripe</a> account or use an existing one.</li>
					<li>
						In your <a href="https://dashboard.stripe.com/" target="_blank">Stripe dashboard</a>. 
						Reveal and copy the secret key and set it as your stripe_api_secret in your Xano workspace
						 by clicking Configure on the extension page.

					</li>
					<li>
						In your stripe dashboard go to <a href="https://dashboard.stripe.com/apikeys">Developers —> API keys</a>. Reveal and copy the secret key and set it as your 
						<code>stripe_api_secret</code> in your Xano workspace environment variables.
					</li>
					<li>
						Optional: To use the webhook functionality, go to 
						<a href="https://dashboard.stripe.com/webhooks">Developers —> Webhooks</a>. Click <b>+ Add Endpoint</b>
						Then set the url to your <code>your_xano_api_url/webhooks</code>
						and add the following event types: 
						<ul>
							<li>price.created</li>
							<li>price.deleted</li>
							<li>price.updated</li>
						</ul>
					</li>
				</ul>
				<h2 class="mt-2">Components</h2>
				<ul>
					<li>
						<b>Prices</b>
						<p>
							This component gives you a paged table of all your Stripe Prices. You can click to view the entire price object in the view panel.
						</p>
					</li>
					<li>
						<b>View Panel</b> 
						<p>
							Here you can view a full-price object. You can also click Edit to modify it.
						</p>
					</li>
					<li>
						<b>Manage Panel</b>
						<p>This where you can create new Prices or edit and delete existing ones.</p>
					</li>
					<li>
						<b>Config Panel</b>
						<p>This is where you’ll set your Xano API URL.</p>
					</li>
				</ul>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/prices',
			'/prices/{id}',
		]
	};

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		@Inject(DOCUMENT) private document
	) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
